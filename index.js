const http = require('http');

http.createServer((req, res) => {
    res.writeHead(200, { 'Content-type': 'text/html' });
    res.end('<html><head><title>Hello world</title></head><body><h1>Pozdravljen svet :) </h1></body></html>')
}).listen(5000, () => {
    console.log('Strežnik posluša na http://localhost:5000');
})